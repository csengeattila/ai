#!/bin/bash

MYUSER=your-user-name               # Your user name
MYHOST=your-host-name               # Host name
USERPASSWORD=your-password          # Password
INSTALLDRIVE=vda                    # Virtual PC
# INSTALLDRIVE=sda                  # Real PC
UCODE=amd-ucode                     # Processor ucode
# UCODE=intel-ucode                 # Processor ucode

####------------------------
#Installing the BASE SYSTEM ------------------------------------------------------
DRIVEPATH=/dev/"$INSTALLDRIVE"      # DO NOT CHANGE ME #
if [ "$(stat -c %d:%i /)" == "$(stat -c %d:%i /proc/1/root/.)" ]
then
  # Update
  pacman -Syyy
  # Syncronysattion
  timedatectl set-ntp true
  
  # Partitioning
  parted -s $DRIVEPATH \
    mklabel gpt \
    mkpart primary fat32 1 1G\
    mkpart primary btrfs 1G 100% \
    set 1 boot on \
    set 2 lvm on

  # Formating 
  mkfs.fat -F32 "$DRIVEPATH"1
  mkfs.btrfs -f -m single -L arch_os "$DRIVEPATH"2

  # Creating the tree
  mount -o compress=lzo "$DRIVEPATH"2 /mnt
  btrfs su cr /mnt/@
  btrfs su cr /mnt/@home
  btrfs su cr /mnt/@log
  btrfs su cr /mnt/@pkg
  btrfs su cr /mnt/@srv
  btrfs su cr /mnt/@tmp
  btrfs su cr /mnt/@snapshots

  # Mounting the volumes
  umount /mnt
  mount -o compress=lzo,subvol=@ "$DRIVEPATH"2 /mnt
  mkdir -p /mnt/{install,boot,home,.snapshots,srv,var/{log,cache/pacman/pkg,tmp}}

  mount "$DRIVEPATH"1 /mnt/boot
  mount -o compress=lzo,subvol=@home "$DRIVEPATH"2 /mnt/home
  mount -o compress=lzo,subvol=@log "$DRIVEPATH"2 /mnt/var/log
  mount -o compress=lzo,subvol=@pkg "$DRIVEPATH"2 /mnt/var/cache/pacman/pkg
  mount -o compress=lzo,subvol=@srv "$DRIVEPATH"2 /mnt/srv
  mount -o compress=lzo,subvol=@tmp "$DRIVEPATH"2 /mnt/var/tmp
  mount -o compress=lzo,subvol=@snapshots "$DRIVEPATH"2 /mnt/.snapshots

  # Installing base packages
  pacstrap /mnt \
            base \
            linux \
            linux-firmware \
            $UCODE \
            base-devel \
            polkit \
            btrfs-progs \
            networkmanager \
            network-manager-applet \
            ntfs-3g \
            efibootmgr \
            cups 
            
  # mounting boot and root 
  genfstab -U /mnt >> /mnt/etc/fstab
  # copying files needed  
  cp $0 /mnt/install/install.sh
  chmod +x /mnt/install/install.sh
	
  # Going into the chroot
  arch-chroot /mnt install/install.sh chroot
else
    # Setting timezone 
    ln -sf /usr/share/zoneinfo/Europe/Budapest /etc/localtime
    hwclock --systohc
     
    # Local settings
    echo "hu_HU.UTF-8 UTF-8" >> /etc/locale.gen
    echo "LANG=hu_HU.UTF-8" >> /etc/locale.conf
    echo "KEYMAP=hu" >> /etc/vconsole.conf
    echo $MYHOST >> /etc/hostname
    echo "127.0.0.1	localhost" >> /etc/hosts
    echo "::1		localhost" >> /etc/hosts
    echo "127.0.1.1	"$MYHOST".localdomain"  $MYHOST >> /etc/hosts
    locale-gen

    # ROOT password 
    echo -en "$USERPASSWORD\n$USERPASSWORD" | passwd
    
    # BTRFS module
    sed -i 's/MODULES=()/MODULES=(btrfs)/' /etc/mkinitcpio.conf
    mkinitcpio -p linux

    # creating systemd boot ---------------------------------------------------------
    PID=$(sudo blkid -s PARTUUID -o value "$DRIVEPATH"2)
    echo "PARTUUID=$PID rw"
    bootctl install
    rm /boot/loader/loader.conf
    echo "timeout	2" >> /boot/loader/loader.conf
    echo "default	arch-*" >> /boot/loader/loader.conf
    echo "title     Arch Linux" >> /boot/loader/entries/arch.conf
    echo "linux     /vmlinuz-linux" >> /boot/loader/entries/arch.conf
    echo "initrd    /amd-ucode.img" >> /boot/loader/entries/arch.conf
    echo "initrd    /initramfs-linux.img" >> /boot/loader/entries/arch.conf
    echo "options   root=PARTUUID=$PID rootflags=subvol=/@ rw rootfstype=btrfs" >> /boot/loader/entries/arch.conf

    # creating user
    useradd -mG wheel $MYUSER
    echo -en "$USERPASSWORD\n$USERPASSWORD" | passwd $MYUSER


    # sudo
    sed -i 's/# %wheel ALL=(ALL:ALL) NOPASSWD: ALL/%wheel ALL=(ALL:ALL) NOPASSWD: ALL/' /etc/sudoers
    sed -i 's/#ParallelDownloads = 5/ParallelDownloads = 50/' /etc/pacman.conf

    systemctl enable NetworkManager
    systemctl enable cups 

    sudo pacman --noconfirm -S \
      neovim

    # Setting up Chaotic AUR
    pacman-key --recv-key 3056513887B78AEB --keyserver keyserver.ubuntu.com
    pacman-key --lsign-key 3056513887B78AEB
    pacman -U 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst' 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'
    
    echo "[chaotic-aur]" >> /etc/pacman.conf
    echo "Include = /etc/pacman.d/chaotic-mirrorlist" >> /etc/pacman.conf

    # Autologin service -------------------------------------------------------------
    # sed -i 's/ExecStart=/# ExecStart=/' /usr/lib/systemd/system/getty@.service
    # sed -i '38i\ExecStart=-/sbin/agetty -i -a '$MYUSER' %I $TERM' /usr/lib/systemd/system/getty@.service
fi

